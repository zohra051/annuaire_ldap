package fr.info.objet;

import java.io.Serializable;

public class Utilisateur implements Serializable{
	private static final long serialVersionUID = 3309572647822157460L;
	
	private String sn;
	private String givenName;
	private String cn;
	private String userPassword;
	private String homeDirectory;
	private int uidNumber;
	private int gidNumber;
	private String loginShell;
	private String uid;
	private String oc;
	
	public Utilisateur() {
		loginShell = "/bin/bash";
	}
	
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getHomeDirectory() {
		return homeDirectory;
	}
	public void setHomeDirectory(String homeDirectory) {
		this.homeDirectory = homeDirectory;
	}
	public int getUidNumber() {
		return uidNumber;
	}
	public void setUidNumber(int uidNumber) {
		this.uidNumber = uidNumber;
	}
	public int getGidNumber() {
		return gidNumber;
	}
	public void setGidNumber(int gidNumber) {
		this.gidNumber = gidNumber;
	}
	public String getLoginShell() {
		return loginShell;
	}
	public void setLoginShell(String loginShell) {
		this.loginShell = loginShell;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
}
