package fr.info.servlet.exportimport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import fr.info.ldap.ExportImport;

/**
 * Servlet implementation class Import
 */
@WebServlet("/Import")
@MultipartConfig
public class Import extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Import() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/import.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Part filePart = request.getPart("fichier"); // Retrieves <input type="file" name="file">
	    InputStream fileContent = filePart.getInputStream();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(fileContent));
	    ExportImport importer = new ExportImport();
	    boolean sucess = importer.importation(reader);
	    if(sucess)
	    	response.sendRedirect("/annuaire/connection_administrateur/administrateur");
	    else {
	    	request.setAttribute("message","le fichier n'est pas correct");
	    	doGet(request,response);
	    }
	}

}
