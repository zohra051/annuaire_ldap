package fr.info.servlet.connection;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.ldap.ConnectionUtilisateur;

/**
 * Servlet implementation class CoUtilisateur
 */
@WebServlet("/CoUtilisateur")
public class CoUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CoUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/connection_utilisateur.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ConnectionUtilisateur connection = new ConnectionUtilisateur();
		String uid = request.getParameter("uid_utilisateur");
		String mdp = request.getParameter("mot_de_passe_utilisateur");
		if(connection.connecter(uid, mdp))
		{
			HttpSession session = request.getSession();
		    session.setAttribute("nom", uid);
			response.sendRedirect("/annuaire/connection_utilisateur/utilisateur");
		}
		else
		{
			System.out.println("pas marché");
			response.sendRedirect("/annuaire/connection_utilisateur");
		}
		
	}

}
