package fr.info.servlet.connection;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.info.ldap.ConnectionAdministrateur;
import fr.info.ldap.ConnectionUtilisateur;

/**
 * Servlet implementation class CoGroupe
 */
@WebServlet("/CoAdministrateur")
public class CoAdministrateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CoAdministrateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/connection_administrateur.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConnectionAdministrateur connection = new ConnectionAdministrateur();
		String mdp = request.getParameter("mot_de_passe_administrateur");
		if(connection.connecter(mdp))
		{
			response.sendRedirect("/annuaire/connection_administrateur/administrateur");
		}
		else
		{
			System.out.println("pas marché");
			response.sendRedirect("/annuaire/connection_administrateur");
		}
		
	}

}
