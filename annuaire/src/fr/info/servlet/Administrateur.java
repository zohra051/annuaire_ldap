package fr.info.servlet;

import java.io.IOException;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.ldap.AfficherAdminGroup;
import fr.info.ldap.AfficherAdminUser;
import fr.info.ldap.AfficherUtilisateur;
import fr.info.ldap.CheckIfUserIsMemberOf;
import fr.info.ldap.ModifierGroupe;
import fr.info.ldap.SupprimerGroupe;
import fr.info.ldap.SupprimerUtilisateur;

/**
 * Servlet implementation class Administrateur
 */
@WebServlet("/Administrateur")
public class Administrateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Administrateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AfficherAdminUser utili = new AfficherAdminUser();
		NamingEnumeration<SearchResult> utilisateurs = utili.afficherAdminUser();
		request.setAttribute("utilisateurs",utilisateurs);
		
		AfficherAdminGroup grp = new AfficherAdminGroup();
		NamingEnumeration<SearchResult> groupes = grp.afficherAdminGroup();
		request.setAttribute("groupes",groupes);
		
	    
		this.getServletContext().getRequestDispatcher("/administrateur.jsp").forward(request, response);
	}

	/** 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("import") !=null) {
			response.sendRedirect("/annuaire/connection_administrateur/administrateur/import");
			return;
		}
		if(request.getParameter("export") != null){
			response.sendRedirect("/annuaire/connection_administrateur/administrateur/export");
			return;
		}
		/*SUPPRIMER UTILISATEUR */
		String uid_utilisateur = request.getParameter("uid_utilisateur");
		String supp_ou_mod = request.getParameter("Supp_ou_mod");
		if(supp_ou_mod.equals("supp"))
		{
			SupprimerUtilisateur supprimerUtilisateur = new SupprimerUtilisateur();
			supprimerUtilisateur.supprimer(uid_utilisateur);
			AfficherAdminGroup grp = new AfficherAdminGroup();
			NamingEnumeration<SearchResult> groupes = grp.afficherAdminGroup(); ;
			try {
				while (groupes.hasMoreElements()) {
					SearchResult sr = (SearchResult) groupes.next();
					Attributes attrs = sr.getAttributes();
					for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
						Attribute attr = (Attribute) ae.next();
						if (attr.getID().equals("cn")) {
							String groupUid = (String) attr.get(0);
							CheckIfUserIsMemberOf check = new CheckIfUserIsMemberOf();
							if(check.check(uid_utilisateur,groupUid)) {
								ModifierGroupe mod = new ModifierGroupe();
								mod.remove(uid_utilisateur, groupUid);
							}
						}
					}
				}
			}catch(Exception e) {e.printStackTrace();}
			
		}
		/*MODIFIER UTILISATEUR */
		if(supp_ou_mod.equals("mod"))
		{
			HttpSession session = request.getSession();
			session.setAttribute("uid_utilisateur",uid_utilisateur);
			response.sendRedirect("/annuaire/connection_administrateur/administrateur/modifier_utilisateur");
		}
		
		/*SUPPRIMER GROUPE*/
		String uid_groupe = request.getParameter("uid_groupe");
		if(supp_ou_mod.equals("supp_grp"))
		{
			SupprimerGroupe supprimerGroupe = new SupprimerGroupe();
			supprimerGroupe.supprimerGroupe(uid_groupe);
		}
		if(supp_ou_mod.equals("mod_grp"))
		{
			HttpSession session = request.getSession();
			session.setAttribute("uid_groupe",uid_groupe);
			response.sendRedirect("/annuaire/connection_administrateur/administrateur/modifier_groupe");
		}

		doGet(request,response);
	}

}
