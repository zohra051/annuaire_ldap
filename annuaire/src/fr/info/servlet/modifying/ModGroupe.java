package fr.info.servlet.modifying;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.ldap.AfficherGroupDesc;
import fr.info.ldap.AfficherGroupMember;
import fr.info.ldap.ModifierGroupe;
import fr.info.ldap.CheckIfUserIsMemberOf;
import fr.info.ldap.CheckUserExist;

/**
 * Servlet implementation class ModGroupe
 */
@WebServlet("/ModGroupe")
public class ModGroupe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModGroupe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String uid = (String) session.getAttribute("uid_groupe");
		AfficherGroupMember aff = new AfficherGroupMember();
		Vector<String> member = aff.get(uid);
		request.setAttribute("member",member);
		AfficherGroupDesc desc = new AfficherGroupDesc();
		request.setAttribute("description", desc.get(uid));
		this.getServletContext().getRequestDispatcher("/modifier_groupe.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userUid = request.getParameter("user_uid");
		HttpSession session = request.getSession();
		String groupUid = (String) session.getAttribute("uid_groupe");
		if (request.getParameter("add") != null){
			CheckUserExist userExist = new CheckUserExist();
			CheckIfUserIsMemberOf memberOf = new CheckIfUserIsMemberOf();
			if(userExist.check(userUid) && !memberOf.check(userUid, groupUid)){
				ModifierGroupe ajouter = new ModifierGroupe();
				ajouter.add(userUid, groupUid);
			}
			else {
				request.setAttribute("message","l'utilisateur existe déja");
			}
		}
		if (request.getParameter("del") != null) {
			CheckIfUserIsMemberOf memberOf = new CheckIfUserIsMemberOf();
			if(memberOf.check(userUid,groupUid)) {
				ModifierGroupe modifier = new ModifierGroupe();
				modifier.remove(userUid, groupUid);
			}
			else {
				request.setAttribute("message","l'utilisateur n'est pas dans ce groupe");
			}	
		}
		if (request.getParameter("desc") != null) {
			String description = request.getParameter("group_describe_mod");
			if(!description.trim().equals("")) {
				ModifierGroupe modifier = new ModifierGroupe();
				modifier.changeDescription(description, groupUid);
			}
		}
		if(request.getParameter("retour") != null)
		{
			response.sendRedirect("/annuaire/connection_administrateur/administrateur");
		}
		doGet(request,response);
	}

}
