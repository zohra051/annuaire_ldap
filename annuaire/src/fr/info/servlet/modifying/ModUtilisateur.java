package fr.info.servlet.modifying;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.ldap.ModifierUtilisateur;

/**
 * Servlet implementation class ModUtilisateur
 */
@WebServlet("/ModUtilisateur")
public class ModUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/modifier_utilisateur.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String uid = (String) session.getAttribute("uid_utilisateur");
		String type = request.getParameter("type");
		if(type.equals("uid"))
		{
			String newNom = request.getParameter("user_name_mod_new");
			if(newNom.indexOf(" ")<=0)
				doGet(request, response);
			else {
				ModifierUtilisateur mod = new ModifierUtilisateur();
				mod.ModifierUid(uid, newNom);
				response.sendRedirect("/annuaire/connection_administrateur/administrateur");
			}
		}
		if(type.equals("password")) 
		{
			ModifierUtilisateur mod = new ModifierUtilisateur();
			String newPass = request.getParameter("password_mod_new");
			mod.ModifierPassword(uid,newPass);
			response.sendRedirect("/annuaire/connection_administrateur/administrateur");
		}
		
		
	}

}
