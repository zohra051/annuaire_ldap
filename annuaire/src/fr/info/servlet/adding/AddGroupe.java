package fr.info.servlet.adding;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.info.ldap.AjouterGroupe;
import fr.info.ldap.AjouterUtilisateur;

/**
 * Servlet implementation class AddGroupe
 */
@WebServlet("/AddGroupe")
public class AddGroupe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddGroupe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/ajouter_groupe.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AjouterGroupe ajouterGroupe = new AjouterGroupe();
		String nom = request.getParameter("group_name");
		String describe = request.getParameter("group_describe");
		ajouterGroupe.ajouter(nom, describe);
		
		
		
		response.sendRedirect("/annuaire/connection_administrateur/administrateur");
	}

}
