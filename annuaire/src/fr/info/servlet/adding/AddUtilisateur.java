package fr.info.servlet.adding;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.info.ldap.AjouterUtilisateur;
import fr.info.ldap.CheckUserExist;

/**
 * Servlet implementation class AddUtilisateur
 */
@WebServlet("/AddUtilisateur")
public class AddUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/ajouter_utilisateur.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AjouterUtilisateur ajouterUtilisateur = new AjouterUtilisateur();
		String nom = request.getParameter("user_name");
		String prenom = request.getParameter("user_surname");
		String password = request.getParameter("user_password");
		CheckUserExist check = new CheckUserExist();
		if(check.check(nom.substring(0,1).toUpperCase()+ prenom.substring(0,1).toUpperCase()+ prenom.substring(1).toLowerCase())) {
			//l'utilisateur existe déja
			request.setAttribute("message","l'utilisateur est déja dans le groupe, où n'existe pas");
			doGet(request,response);
		}
		ajouterUtilisateur.ajouter(nom, prenom, password);
		
		
		
		response.sendRedirect("/annuaire/connection_administrateur/administrateur");
	}

}
