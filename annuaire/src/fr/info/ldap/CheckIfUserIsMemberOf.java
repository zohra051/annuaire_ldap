package fr.info.ldap;

import java.util.Properties;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class CheckIfUserIsMemberOf {
	public CheckIfUserIsMemberOf() {}
	
	public boolean check(String userUid, String groupUid) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext ctx = new InitialDirContext(env);
			NamingEnumeration result = null;
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			result = ctx.search("ou=group,dc=jimmy","cn="+groupUid,controls);
			while(result.hasMore()) {
				SearchResult searchResult = (SearchResult) result.next();
				Attributes attributes = searchResult.getAttributes();
				Attribute attr = attributes.get("memberUid");
				for(int i=0;i<attr.size();i++) {
					if(attr.get(i).equals(userUid)) {
						return true;
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
