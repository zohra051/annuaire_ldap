package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class AfficherUtilisateur {
	
	public Attributes afficherUser(String nom) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
	
		try {
			// LdapContext
			DirContext ctx = new InitialDirContext(env);
			Attributes attrs = ctx.getAttributes("UID="+nom+",OU=people,DC=jimmy", null);
			return attrs;
		}
		catch(Exception e) {e.printStackTrace();return null;}				
	}
	
	
	
	 

}
