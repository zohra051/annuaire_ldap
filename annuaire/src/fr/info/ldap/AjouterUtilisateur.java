package fr.info.ldap;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import fr.info.objet.Utilisateur;

public class AjouterUtilisateur {
	public AjouterUtilisateur() {}
	
	public void ajouter(String nom,String prenom,String password) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		DirContext dirContext;
		
		Attribute sn = new BasicAttribute("sn",nom.toUpperCase());
		Attribute givenName = new BasicAttribute("givenName",prenom.substring(0,1).toUpperCase()+prenom.substring(1));
		Attribute cn = new BasicAttribute("cn",nom.toUpperCase()+" "+prenom.substring(0,1).toUpperCase()+prenom.substring(1));
		Attribute pw = new BasicAttribute("userPassword",password);
		Attribute uid = new BasicAttribute("uid",nom.substring(0,1).toUpperCase()+prenom.substring(0,1).toUpperCase()+prenom.substring(1));
		int random = (int) Math.random()*12000;
		Attribute uidNumber = new BasicAttribute ("uidNumber",Integer.toString(random));
		Attribute oc = new BasicAttribute("objectClass");
		oc.add("top");
		oc.add("person");
		oc.add("organizationalPerson");
        oc.add("inetOrgPerson");
        
        
		try {
			dirContext = new InitialDirContext(env);
	        Attributes entry = new BasicAttributes();
	        entry.put(sn);
	        entry.put(givenName);
	        entry.put(cn);
	        entry.put(pw);
	        entry.put(uid);
	        //entry.put(uidNumber);
	        entry.put(oc);
	        dirContext.createSubcontext("uid="+nom.substring(0,1).toUpperCase()+prenom.substring(0,1).toUpperCase()+prenom.substring(1)+",ou=people,dc=jimmy",entry);
	        System.out.println("hokey");
		
		}catch(Exception e) {e.printStackTrace();}
	}
}
