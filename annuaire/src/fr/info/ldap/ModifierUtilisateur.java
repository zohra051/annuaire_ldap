package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;

public class ModifierUtilisateur {
	public ModifierUtilisateur() {}
	
	public void ModifierUid (String uid,String newNom) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext dirContext =  new InitialDirContext(env) ;
			ModificationItem[] modif = new ModificationItem[3];
			
			String[] strings = newNom.split(" ");
			
			Attribute cn = new BasicAttribute("cn");
			cn.add(strings[0].toUpperCase()+" "+strings[1].substring(0,1).toUpperCase()+strings[1].substring(1).toLowerCase());
			modif[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,cn);
			
			Attribute sn = new BasicAttribute("sn");
			sn.add(strings[0].toUpperCase());
			modif[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,sn);
			
			Attribute givenName = new BasicAttribute("givenName");
			givenName.add(strings[1].substring(0,1).toUpperCase()+strings[1].substring(1).toLowerCase());
			modif[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,givenName);
			
			String newUid= strings[0].substring(0,1).toUpperCase()+strings[1].substring(0,1).toUpperCase()+strings[1].substring(1).toLowerCase();
			
			dirContext.modifyAttributes("uid="+uid+",ou=people,dc=jimmy",modif);
			dirContext.rename("uid="+uid+",ou=people,dc=jimmy","uid="+newUid+",ou=people,dc=jimmy");
			dirContext.close();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	public void ModifierPassword(String uid,String newPass) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldaps://localhost:636");
		env.put(Context.SECURITY_PROTOCOL, "ssl");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext dirContext =  new InitialDirContext(env) ;
			 Attributes attributes = new BasicAttributes(true); 
		     Attribute password = new BasicAttribute("userPassword"); 
		     password.add(newPass);
		     attributes.put(password); 
			dirContext.modifyAttributes("uid="+uid+",ou=people,dc=jimmy",DirContext.REPLACE_ATTRIBUTE,attributes);
			dirContext.close();
		}
		catch(Exception e) {e.printStackTrace();}
	}
}
