package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;

public class AfficherAdminGroup {
	
	public AfficherAdminGroup() {}

	
	public NamingEnumeration<SearchResult>  afficherAdminGroup() {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
	
		try {
			DirContext ctx = new InitialDirContext(env);
			NamingEnumeration<SearchResult> grp = ctx.search("OU=group,DC=jimmy", null);
			 return grp;
			
		}
		catch(Exception e) {e.printStackTrace(); return null;}	
	}
}
