package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class AjouterGroupe {

	public AjouterGroupe() {
	
	}
	
	public void ajouter(String nom,String describe) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		DirContext dirContext;
		
	
		Attribute cn = new BasicAttribute("cn",nom);
		Attribute desc = new BasicAttribute("description",describe);
		int random = (int) Math.random()*12000;
		Attribute gidNumber = new BasicAttribute ("gidNumber",Integer.toString(random));
		Attribute oc = new BasicAttribute("objectClass");
		oc.add("top");
		oc.add("posixGroup");
        
        
		try {
			dirContext = new InitialDirContext(env);
	        Attributes entry = new BasicAttributes();
	        entry.put(cn);
	        entry.put(gidNumber);
	        entry.put(desc);
	        entry.put(oc);
	        dirContext.createSubcontext("cn="+nom+",ou=group,dc=jimmy",entry);
	        System.out.println("hokey");
		
		}catch(Exception e) {e.printStackTrace();}
	}
	
}
