package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class SupprimerUtilisateur {
	
	
	public SupprimerUtilisateur() {}
	
	public void supprimer(String nom) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext dirContext =  new InitialDirContext(env) ;
			dirContext.destroySubcontext("uid="+nom+",ou=people,dc=jimmy");
		}catch (Exception e) {e.printStackTrace();}
		
	}

}
