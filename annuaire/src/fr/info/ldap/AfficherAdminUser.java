package fr.info.ldap;

import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;

public class AfficherAdminUser {
	
	public AfficherAdminUser(){
	
	}
	
	public NamingEnumeration<SearchResult>  afficherAdminUser() {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
	
		try {
			DirContext ctx = new InitialDirContext(env);
			NamingEnumeration<SearchResult> user = ctx.search("OU=people,DC=jimmy", null);
			 return user;
			
		}
		catch(Exception e) {e.printStackTrace(); return null;}	
	}
}
