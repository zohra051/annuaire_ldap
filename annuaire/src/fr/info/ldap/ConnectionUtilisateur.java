package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class ConnectionUtilisateur {
	
	public ConnectionUtilisateur() {}
	
	public boolean connecter(String uid, String password) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"uid="+uid+",ou=people,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,password);
		DirContext dirContext;
		try {
			dirContext = new InitialDirContext(env);
			return true;
		} 
		catch(Exception e) 
		{ 
			return false;
		}
	}

}
