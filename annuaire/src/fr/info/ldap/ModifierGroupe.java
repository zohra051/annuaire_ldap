package fr.info.ldap;

import java.util.Properties;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;

public class ModifierGroupe {
	public ModifierGroupe() {}
	
	public void add(String userUid,String groupUid) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext ctx = new InitialDirContext(env);
			Attributes attributes = new BasicAttributes(true); 
		    Attribute attribut = new BasicAttribute("memberUid"); 
		    attribut.add(userUid); 
		    attributes.put(attribut);
		    ctx.modifyAttributes("cn="+groupUid+",ou=group,dc=jimmy",DirContext.ADD_ATTRIBUTE,attributes); 
		    ctx.close(); 
		}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void remove(String userUid, String groupUid)
	{
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext ctx = new InitialDirContext(env);
			ModificationItem mod[] = new ModificationItem[1];
			mod[0] = new ModificationItem(InitialDirContext.REMOVE_ATTRIBUTE,new BasicAttribute("memberUid",userUid));
			ctx.modifyAttributes("cn="+groupUid+",ou=group,dc=jimmy",mod);
			ctx.close();
		}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public void changeDescription(String description, String groupUid) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext ctx = new InitialDirContext(env);
			ModificationItem mod[] = new ModificationItem[1];
			mod[0] = new ModificationItem(InitialDirContext.REPLACE_ATTRIBUTE,new BasicAttribute("description",description));
			ctx.modifyAttributes("cn="+groupUid+",ou=group,dc=jimmy",mod);
			ctx.close();
		}
		catch(Exception e) {e.printStackTrace();}
	}
}
