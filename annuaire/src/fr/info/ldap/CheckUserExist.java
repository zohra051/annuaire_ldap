package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class CheckUserExist {
	public CheckUserExist() {}
	
	public boolean check(String uid) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
		try {
			DirContext ctx = new InitialDirContext(env);
			Attributes attrs = ctx.getAttributes("UID="+uid+",OU=people,DC=jimmy", null);
			return true;
		}
		catch(Exception e) {return false;}
	}
}
