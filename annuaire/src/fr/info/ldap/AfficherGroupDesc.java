package fr.info.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class AfficherGroupDesc {
	public AfficherGroupDesc() {}
	
	public String get(String groupUid) {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://localhost:389");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,"cn=admin,dc=jimmy");
		env.put(Context.SECURITY_CREDENTIALS,"bla");
	
		try {
			// LdapContext
			DirContext ctx = new InitialDirContext(env);
			Attributes attrs = ctx.getAttributes("CN="+groupUid+",OU=group,DC=jimmy", null);
			return attrs.get("description").toString();
		}
		catch(Exception e) {e.printStackTrace();return null;}	
	}
}
