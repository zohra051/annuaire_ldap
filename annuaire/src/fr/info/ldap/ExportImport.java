package fr.info.ldap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

public class ExportImport {
	public ExportImport() {}
	
	public File export() {
		File result = new File("ldap.txt");
		try {	
			FileWriter writer = new FileWriter(result);
			
			String Newligne=System.getProperty("line.separator");
			
			AfficherAdminUser adminUsers = new AfficherAdminUser();
			NamingEnumeration<SearchResult> users = adminUsers.afficherAdminUser();
			writer.write("USERS");
			writer.write(Newligne);
			while(users.hasMoreElements()) {
				SearchResult sr = (SearchResult) users.next();
				Attributes attrs = sr.getAttributes();
				writer.write(attrs.get("sn").get()+" ");
				writer.write(attrs.get("givenName").get()+" ");
				writer.write(new String((byte[])attrs.get("userPassword").get()));
				writer.write(Newligne);
			}
			
			AfficherAdminGroup adminGroup = new AfficherAdminGroup();
			NamingEnumeration<SearchResult> groups = adminGroup.afficherAdminGroup();
			writer.write("GROUPS");
			writer.write(Newligne);
			while(groups.hasMoreElements()) {
				SearchResult sr = (SearchResult) groups.next();
				Attributes attrs = sr.getAttributes();
				String groupUid = (String) attrs.get("cn").get();
				writer.write(groupUid +" ");
				writer.write(attrs.get("description").get()+" ");
				AfficherGroupMember afficherMembre = new AfficherGroupMember();
				Vector<String> members = afficherMembre.get(groupUid);
				for(int i=0;i<members.size();i++) {
					writer.write(members.get(i)+" ");
				}
				writer.write(Newligne);
			}
			writer.close();
		}
		catch(Exception e) {e.printStackTrace();}
		return result;
	}
	
	public boolean importation(BufferedReader reader) {
	    try 
	    {
			if(reader.ready()) {
				//check si la première ligne commance par USERS ou GROUPS
			    String line = reader.readLine();
			    String[] words = line.split(" ");
			    AjouterUtilisateur user = new AjouterUtilisateur();
			    AjouterGroupe group = new AjouterGroupe();
			    ModifierGroupe member = new ModifierGroupe();
			    if(words[0].equals("USERS")) {
			    	while(reader.ready()) {
				    	line = reader.readLine();
				    	words = line.split(" ");
				    	if(words[0].equals("GROUPS"))
				    		break;
				    	if(words.length != 3)
				    		return false;
				    	user.ajouter(words[0], words[1], words[2]);
				    }
			    }
			    if(words[0].equals("GROUPS")) {
			    	while(reader.ready()) {
				    	line = reader.readLine();
				    	words = line.split(" ");
				    	if(words.length < 2)
				    		return false;
				    	group.ajouter(words[0],words[1]);
				    	for(int i=2;i<words.length;i++) {
				    		member.add(words[i],words[0]);
				    	}
			    	}
			    }
			}
		} catch (IOException e) {e.printStackTrace(); return false;}
	    return true;
	}
}
