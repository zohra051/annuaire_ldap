<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ page import="javax.naming.NamingEnumeration"%>
    <%@ page import="javax.naming.directory.SearchResult"%>
    <%@ page import="javax.naming.directory.Attribute" %>
	<%@ page import="javax.naming.directory.Attributes"%>
	<%@ page import="javax.naming.NamingException"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Annuaire</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
</head>
<body>
<div class="center_milieu2">
	<form method="get" action="/annuaire/connection_administrateur/administrateur/ajouter_utilisateur">
		<div style="display:flex">
			<label> UTILISATEURS</label>
			<input type="submit" value="+">
		</div>
	</form>

<%
NamingEnumeration<SearchResult> users = (NamingEnumeration<SearchResult>) request.getAttribute("utilisateurs");

//Parcourir tous les Utilisateurs
while (users.hasMoreElements()) {
	SearchResult sr = (SearchResult) users.next();
	Attributes attrs = sr.getAttributes();

	if (attrs != null) {
		try {
			//Récupération des attributs
			for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
				Attribute attr = (Attribute) ae.next();
				if (attr.getID().equals("uid")) {
					out.println("<div class=\"utilisateur_groupe\" style=\"display:flex\">");
						out.println("<label>"+attr.get(0).toString()+"</label>");
						out.println("<tab>");
						out.println("<form method=\"post\">");
						out.println("<input type=\"HIDDEN\" name=\"uid_utilisateur\" value=\""+attr.get(0).toString()+"\"/>");
							out.println("<input type=\"HIDDEN\" name=\"Supp_ou_mod\" value=\"mod\"/>");
							out.println("<input type=\"submit\" value=\"Modifier\">");
						out.println("</form>");	
						
						out.println("<form method=\"post\">");
							out.println("<input type=\"HIDDEN\" name=\"uid_utilisateur\" value=\""+attr.get(0).toString()+"\"/>");
							out.println("<input type=\"HIDDEN\" name=\"Supp_ou_mod\" value=\"supp\"/>");
							out.println("<input type=\"submit\" value=\"Supprimer\">");
						out.println("</form>");	
					out.println("</div>");
				}
			}
		} catch (NamingException e) {
			System.err.println("Défaut : " + e);
			e.printStackTrace();
		}
	}
}

%>

	

		
	<br><br>
		
	<!-- GROUPE -->	
	<form method="get" action="/annuaire/connection_administrateur/administrateur/ajouter_groupe">
		<div style="display:flex">
			<label> GROUPES </label>
			<input type="submit" value="+">
		</div>
	</form>

	<%
NamingEnumeration<SearchResult> groups = (NamingEnumeration<SearchResult>) request.getAttribute("groupes");

//Parcourir tous les Groupes
while (groups.hasMoreElements()) {
	SearchResult sr = (SearchResult) groups.next();
	Attributes attrs = sr.getAttributes();

	if (attrs != null) {
		try {
			//Récupération des attributs
			for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
				Attribute attr = (Attribute) ae.next();
				if (attr.getID().equals("cn")) {
					out.println("<div class=\"utilisateur_groupe\" style=\"display:flex\">");
						out.println("<label>"+attr.get(0).toString()+"</label>");
						out.println("<tab>");
						out.println("<form method=\"post\">");
							out.println("<input type=\"HIDDEN\" name=\"uid_groupe\" value=\""+attr.get(0).toString()+"\"/>");
							out.println("<input type=\"HIDDEN\" name=\"Supp_ou_mod\" value=\"mod_grp\"/>");
							out.println("<input type=\"submit\" value=\"Modifier\">");
						out.println("</form>");	
						out.println("<form method=\"post\">");
							out.println("<input type=\"HIDDEN\" name=\"uid_groupe\" value=\""+attr.get(0).toString()+"\"/>");
							out.println("<input type=\"HIDDEN\" name=\"Supp_ou_mod\" value=\"supp_grp\"/>");
							out.println("<input type=\"submit\" value=\"Supprimer\">");
						out.println("</form>");	
					out.println("</div>");
				}
			}
		} catch (NamingException e) {
			System.err.println("Défaut : " + e);
			e.printStackTrace();
		}
	}
}

%>


<br><br>
	<center>
		<form method="post">
		<input type="submit" name="import" value="Importer">
	</form>
	<form method="post">
		<input type="submit" name="export"value="Exporter">
	</form>
		</center>
	
	</div>
</body>
</html>