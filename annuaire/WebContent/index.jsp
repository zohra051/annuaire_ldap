<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Annuaire</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
</head>
<body>
	<div class="center_milieu">
		<center>
		<button><a href="/annuaire/connection_administrateur"> Administrateur </a> </button>
		<br>
		ou
		<br>
		<button><a href="/annuaire/connection_utilisateur"> Utilisateur </a></button>
		</center>
	</div>
</body>
</html>