<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Annuaire</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
</head>
<body>
<!-- MODIFIER UN UTILISATEUR -->
		
			<label>Modifier un utilisateur :
			<%
				
				String uid = (String) session.getAttribute("uid_utilisateur");
				out.println(uid);
			%></label>			
			<br><br>
			<div>
					<div style="display:flex">
						<label> NOM prenom : </label>
						<form method="post">
							<input type="HIDDEN" name="type" value="uid"/>
							<input type="text" name="user_name_mod_new" placeholder="HARRAT Zohra" required>
							<input type="submit" value="Ok">
						</form>
					</div>
					<div style="display:flex">
						<label> Nouveau Mot de Passe : </label>
						<form method="post">
							<input type="HIDDEN" name="type" value="password"/>
							<input type="text" name="password_mod_new" required>
							<input type="submit" value="Ok">
						</form>
					</div>
			</div>
</body>
</html>