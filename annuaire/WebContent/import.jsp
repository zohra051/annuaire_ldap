<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Annuaire</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
</head>
<body>
<div class="center_milieu">
	<form method="post" enctype="multipart/form-data">
        Emplacement du fichier : <input type="file" name="fichier" required/>
        <br>
        <input type="submit" value="Envoyer">
	</form>
	<br><br>
	<% 
				String mess = (String) request.getAttribute("message");
				if(mess != null)
					out.print(mess);
			%>
</div>
</body>
</html>