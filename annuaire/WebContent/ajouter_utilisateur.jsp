<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Annuaire</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
</head>
<body>

<!-- AJOUTER UN UTILISATEUR -->
		<form method="Post">
			<label>Ajouter un utilisateur :</label>			
			<br><br>
			<div style="display:flex">
				<div >
					<input type="text" name="user_name" placeholder="Harrat" >
					<input type="text" name="user_surname" placeholder="Zohra" >
					<input type="text" name="user_password" placeholder="Password">
				</div>
				<input type="submit" value="Ok">
			</div>
			<br>
			<% 
				String mess = (String) request.getAttribute("message");
				if(mess != null)
					out.print(mess);
			%>
		</form>

</body>
</html>