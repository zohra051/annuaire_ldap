e=========  LDAP ========== 

Installation et configuration du serveur OpenLDAP
Utilisation des outils serveurs et des outils clients

Master I2L 
Master ISIDIS

Jérémy WILLIAME
mail :  williamejeremy@gmail.com
IRC:    irc.freenode.net
Salon:  #masteri2l 
Pseudo: Sabaku

=== 1 Installation d'OpenLDAP ===
Comme pour tout logiciel, il est possible d'installer OpenLDAP par le biais de paquets binaires fournis par
une distribution, ou bien en compilant les sources.
Les outils clients sont souvent dissociés des outils serveur et fournis dans des paquets séparés. Nous allons
ici installer les deux sur la même machine, le serveur LDAP. Ceci n'est pas forcément le cas dans un
environnement de production où les clients agissent à distance depuis une autre machine.
Sur une distribution Debian, les paquets à installer sont les suivants : slapd et ldap-utils.

# apt-get install slapd ldap-utils

Une fenêtre apparaît alors et vous demande le mot de passe associé à l'annuaire que vous mettez en place.
Reconfigurer le paquet:

# dpkg-reconfigure slapd

A l'installation du paquet slapd, une fenêtre a dû s'ouvrir demandant les paramètres suivants :
Voulez-vous omettre la configuration d'OpenLDAP ? : <Non>
Nom de domaine : bla.com
Nom d'entité (« organization ») : bla
Mot de passe de l'administrateur : bla
Module de base de données à utiliser : HDB
Faut-il supprimer la base de données à la purge du paquet ? : <Non>
Faut-il déplacer l'ancienne base de données ? : <Oui>
Faut-il autoriser le protocole LDAPv2 ? : <Non>


=== 2 Configuration du serveur ===
2.1 Le démon slapd (Stand-alone LDAP Daemon)
A l'origine, l'intégralité de la configuration du serveur OpenLDAP (le démon slapd) s'effectuait en modifiant
le fichier slapd.conf, situé dans le répertoire /etc/ldap/.
Cependant, depuis la version 2.4 d'OpenLDAP, la méthode de configuration par défaut est d'utiliser le
backend dynamique de configuration slapd-config.
Grâce au backend slapd-config, il n'est plus nécessaire de redémarrer le démon slapd pour qu'un changement
dans la configuration soit prise en compte (modifications à chaud). De plus il est devenu possible d'accéder à
la configuration en interrogeant la branche cn=config via un client LDAP classique.
La configuration du l'annuaire LDAP via slapd-config est stockée dans une base de données LDIF (LDAP Data Interchange Format), c'est à
dire dans une arborescence de répertoires et de fichiers au format LDIF. Sous Debian, et dans les
distributions basées sur Debian, la base de donnée LDIF est située dans le répertoire /etc/ldap/slapd.d/.
Le lancement du serveur s'effectue en utilisant la commande slapd accompagnée des arguments adéquats.
Tapez la commande suivante pour connaître les paramètres par défaut :

# ps aux | grep slapd

La liste de tous les paramètres disponibles sont dans les pages du manuel du démon slapd :

# man slapd

A noter que la configuration par le fichier slapd.conf, bien qu'officiellement devenue obsolète, est toujours
possible en remplaçant l'option « -F <slapd-config-directory> » par l'option « -f <slapd-config-file> ».
Les paramètres du démon slapd sont dans le fichier /etc/default/slapd. Il suffit de l'éditer pour en changer.

2.2 Configuration de l'accès au DIT de configuration (Directory Information Tree)
Avec l'utilisation du backend slapd-config, il faut bien comprendre que l'annuaire que vous venez d'installer
dispose de deux DIT (Directory Information Tree) distincts, c'est à dire de deux arborescences distinctes :
• une arborescence de configuration de l'annuaire (qui remplace le fichier slapd.conf),
• une arborescence contenant les données de l'annuaire proprement dites.
Chacune de ces arborescences a donc sa propre racine (appelée également suffixe), son propre
administrateur, ses propres listes d'accès (ACL), etc., et sont toutes les deux joignables par des clients LDAP,
que ce soit en ligne de commande ou avec une interface graphique.
Veuillez noter que, bien que l'arborescence de configuration soit stockée dans une base de données LDIF,
c'est à dire dans une arborescence de répertoires et de fichiers texte (au format LDIF), il n'est pas
recommandé d'éditer directement ces fichiers. Toute modification que vous effectuerez devra être réalisée
par des opérations LDAP, par exemple à travers des outils comme ldapadd, ldapdelete ou ldapmodify.
La racine de l'arborescence de configuration est « cn=config ». Vous trouverez donc directement sous
/etc/ldap/slapd.d/ le répertoire « cn=config » et le fichier « cn=config.ldif » qui décrit cette entrée :

# cat /etc/ldap/slapd.d/cn\=config.ldif

Le répertoire /etc/ldap/slapd.d/cn=config/ contient lui-même divers répertoires et fichiers LDIF. Les
paramètres de configuration des bases de données sont dans les fichiers LDIF préfixés de « olcDatabase »
(olc = OpenLDAP Configuration), avec un chiffre entre accolades ({x}) pour forcer un ordre dans la lecture
de la configuration.
Vous pouvez ainsi consulter les paramètres de configuration pour accéder à la base de données LDIF :

# cat /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{0\}config.ldif

Par défaut, vous pouvez remarquer que seul le DN de l'administateur (attribut olcRootDN) est renseigné :
« cn=admin,cn=config ». Nous allons donc ajouter le mot de passe de l'administateur (attribut olcRootPw)
sans lequel il n'est pas possible d'accéder à cette arborescence autrement que par LDAP.
Il est possible de renseigner le mot de passe en clair, mais il est préférable de le chiffrer à partir de la
commande slappasswd :

# slappasswd -s bla

L'algorithme de chiffrement par défaut de la commande « slappasswd » est SSHA, mais vous pouvez en
changer avec l'option « -h ».
Le mot de passe « bla » donné ici en exemple n'est évidemment pas un mot de passe à mettre sur un serveur
LDAP en production, car non sécurisé.
A ne pas confondre avec le mot de passe « bla » que nous avons renseigné pour l'arborescence des données,
déjà stocké chiffré en SSHA dans le fichier LDIF « olcDatabase={1}hdb.ldif ».
Créez le fichier LDIF suivant (par l'éditeur de texte que vous préférez, ici vi) :

# vim ~/db_config.ldif

Ajoutez les lignes suivantes (votre empreinte de mot de passe est probablement différente) :

dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {SSHA}nKd4opzliZPF6d0RA/BWAQIVvykV6UXP

Enfin apportez les modifications grâce à la commande :

# ldapadd -Y EXTERNAL -H ldapi:/// -f ~/db_config.ldif

Remarque : l'URI LDAP fonctionne dans un terminal en root en local sur le serveur grâce à l'ACL par défaut
dans le fichier « olcDatabase={0}config.ldif ».
Vérifiez que la modification a bien eu lieu :

# grep Root /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{0\}config.ldif 

olcRootDN: cn=admin,cn=config
olcRootPW:: e1NTSEF9bktkNG9wemxpWlBGNmQwUkEvQldBUUlWdnlrVjZVWFA=


Notez que l'empreinte du mot de passe SSHA est codée en Base64, comme c'est le cas dans le format LDIF
pour toute valeur d'attribut comportant un caractère spécial.
Enfin, vérifiez que vous arrivez bien à vous connecter à votre arborescence de configuration en ligne de
commande ou en configurant un client LDAP graphique.
Exemple de recherche en ligne de commande, pouvant se faire à distance, sans filtre de recherche, à partir de
la racine de l'arborescence, en se connectant avec le DN et le mot de passe de l'administrateur :

# ldapsearch -x -h localhost -p 389 -b cn=config -D cn=admin,cn=config -w bla


2.3/ Le choix du backend de stockage
Les backends sont des modules qui permettent de stocker ou de rechercher des données suite à des requêtes
LDAP .
Il existe 3 catégories de backends :
- Stockage des données (BDB, HDB, ...)
- Proxy (ldap, meta, ...)
- Dynamiques (slapd-config, ...)
Bien souvent le terme de backend est employé pour définir le type de base de données dans laquelle sera
stocké l'annuaire. Pour être précis, un backend est un type d'interface de stockage et une base de données est
une instance d'un backend.
Les backends de stockage recommandés par OpenLDAP sont BDB et HDB, qui sont des « Oracle Berkeley
Database » . La principale différence est que HDB est une variante de BDB qui utilise une structure de base
de données hiérarchique qui supporte le renommage des sous-arbres :
# man slapd-hdb
Le fichier LDIF correspondant au backend dans le DIT de 


2.4/ Les schemas
LDAP utilise des schémas pour le stockage des informations. Ils se situent dans le
dossier /etc/ldap/slapd.d/cn\=config/cn\=schema
# ls /etc/ldap/slapd.d/cn\=config/cn\=schema
cn={0}core.ldif  cn={1}cosine.ldif  cn={2}nis.ldif  cn={3}inetorgperson.ldif

Les schémas listés sont nécessaires à LDAP pour fonctionner. Il ne faut pas les modifier, sauf
si vous savez ce que vous faites :).

J'ai créé un schema pour le TP:
Copiez les deux fichiers dans /etc/ldap/schema/
# cat gamer.schema
attributetype ( 1.3.6.1.4.1.5923.1.1.1.1
        NAME 'pseudo'
        DESC 'Gamer pseudo'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' SINGLE-VALUE )
attributetype ( 1.3.6.1.4.1.5923.1.1.1.2
        NAME 'games'
        DESC 'Games played by gamer'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )
attributetype ( 1.3.6.1.4.1.5923.1.1.1.3
        NAME 'age'
        DESC 'eduPerson per Internet2 and EDUCAUSE'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.27' SINGLE-VALUE )
attributetype ( 1.3.6.1.4.1.5923.1.1.1.4
        NAME 'steamAccount'
        DESC 'Steam account'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )
objectclass ( 1.3.6.1.4.1.5923.1.1.2
        NAME 'gamer'
        SUP inetOrgPerson
        MUST ( pseudo )
        MAY ( games $ age $ steamAccount )
        )

# cat gamer.conf 
include          /etc/ldap/schema/core.schema
include          /etc/ldap/schema/cosine.schema
include          /etc/ldap/schema/nis.schema
include          /etc/ldap/schema/inetorgperson.schema
include          /etc/ldap/schema/gamer.schema


et ajoutez le:
# cp gamer.* /etc/ldap/schema/
# mkdir /tmp/slapd.d/
# cd /etc/ldap/schema/
# slaptest -f gamer.conf -F /tmp/slapd.d/
# cp /tmp/slapd.d/cn\=config/cn\=schema/cn\=\{4\}gamer.ldif "/etc/ldap/slapd.d/cn=config/cn=schema"
# chown openldap.openldap "/etc/ldap/slapd.d/cn=config/cn=schema/cn={4}gamer.ldif"
# /etc/init.d/slapd restart


Le schéma a été ajouté à notre annuaire. Vous pouvez reproduire cette procédure avec les schémas que vous
voulez.

=== 3 Administration du serveur ===
Avec l'installation du démon slapd, vous avez installé un certain nombre d'outils en ligne de commande
permettent d'agir directement au niveau du serveur OpenLDAP, notamment au niveau de sa base de données.
Outils de manipulation de la base gérée par OpenLDAP :
- slapadd : ajoute des entrées au format LDIF dans la base
- slapcat : exporte des données de la base au format LDIF
- slapindex : regénère les index au sein de la base
- slappasswd : génère un mot de passe selon l'algorithme de chiffrement spécifié
Outils de test/validation :
- slapacl : teste qu'une entrée (via son DN) accède bien à des données de l'annuaire selon des directives d'ACL
- slapauth : teste le comportement du serveur sur les authentifications et les autorisations via la prise d'identité
- slapdn : teste la conformité d'un DN
- slaptest : teste la validité de la configuration (dans le fichier slapd.conf pour les anciennes version
d'OpenLDAP ou dans le backend slapd-config pour les versions actuelles)

=== 4 Outils client ===
Avec l'installation du paquet ldap-utils, vous avez installé un certain nombre d'outils en ligne de commande
qui utilisent le protocole LDAP pour agir sur l'annuaire. Ils agissent en tant que clients LDAP standard.
Outils en ligne de commande liés au client :
• ldapmodify : modifie une entrée (ajout d'une entrée ; ajout, modification, suppression d'attributs),
souvent par fichier LDIF
• ldapadd (= ldapmodify -a) : ajoute une entrée
• ldapdelete : supprime une entrée
• ldappasswd : modifie le mot de passe d'une entrée
• ldapmodrdn : modifie le RDN d'une entrée (la renomme)
• ldapsearch : effectue une recherche au sein de l'annuaire
• ldapcompare : permet de comparer l'attribut d'une entrée à une valeur spécifiée
• ldapurl : compose ou décompose une URL LDAP
• ldapwhoami : affiche avec quel utilisateur la connexion au serveur (bind) a eu lieu
• ldapexop : pour les opérations étendues de LDAPv3
A la différence des outils slap(...), les outils ldap(...) utilisent le protocole LDAP, ils peuvent donc être
mis en œuvre depuis n'importe quelle machine disposant d'un accès réseau au serveur LDAP. Ils
utilisent bien évidemment le format LDIF pour échanger des informations avec le serveur.


=== 5 Let's go ===
5.1/ La recherche
Nous allons commencer par afficher notre annuaire via la commande slapcat:
# slapcat 
dn: dc=bla,dc=com
objectClass: top
objectClass: dcObject
objectClass: organization
o: bla
dc: bla
structuralObjectClass: organization
entryUUID: 880f3200-1cb8-1034-8b9a-6ba902e8507f
creatorsName: cn=admin,dc=bla,dc=com
createTimestamp: 20141220172234Z
entryCSN: 20141220172234.621133Z#000000#000#000000
modifiersName: cn=admin,dc=bla,dc=com
modifyTimestamp: 20141220172234Z

dn: cn=admin,dc=bla,dc=com
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
userPassword:: e1NTSEF9emxNNGJSZGI1cjZWMGJFU0RRbkYzSnRSV3haV1NKOW4=
structuralObjectClass: organizationalRole
entryUUID: 881e58f2-1cb8-1034-8b9b-6ba902e8507f
creatorsName: cn=admin,dc=bla,dc=com
createTimestamp: 20141220172234Z
entryCSN: 20141220172234.720435Z#000000#000#000000
modifiersName: cn=admin,dc=bla,dc=com
modifyTimestamp: 20141220172234Z

Et maintenant via la commande client ldapsearch:
# ldapsearch -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -b dc=bla,dc=com -w bla

# extended LDIF
#
# LDAPv3
# base <dc=bla,dc=com> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# bla.com
dn: dc=bla,dc=com
objectClass: top
objectClass: dcObject
objectClass: organization
o: bla
dc: bla

# admin, bla.com
dn: cn=admin,dc=bla,dc=com
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
userPassword:: e1NTSEF9emxNNGJSZGI1cjZWMGJFU0RRbkYzSnRSV3haV1NKOW4=

# search result
search: 2
result: 0 Success

# numResponses: 3
# numEntries: 2


Personnellement j'utilise que la commande ldapsearch.


5.2/ Ajouter des groupes
Créez un ldif:
# cat groups.ldif 
dn: ou=group,dc=bla,dc=com
objectClass: organizationalUnit
ou: group
description: Unix group 

dn: ou=people,dc=bla,dc=com
objectClass: organizationalUnit
ou: people
description: Unix user

et ajoutez le à votre annuaire:
# ldapadd -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f groups.ldif -w bla
adding new entry "ou=group,dc=bla,dc=com"

adding new entry "ou=people,dc=bla,dc=com"


Vérifiez l'ajout des groupes via la commande de recherche de votre choix.

5.3/ Ajouter des entrées
Voici notre première entrée dans l'annuaire:
# cat bender.ldif 
dn: uid=bender,ou=people,dc=bla,dc=com
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
sn: RODRIGUEZ
givenName: Bender
cn: RODRIGUEZ Bender
userPassword: bender
homeDirectory: /home/bender
uidNumber: 1101
gidNumber: 1101
loginShell: /bin/bash
uid: bender

# ldapadd -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f bender.ldif -w bla
adding new entry "uid=bender,ou=people,dc=bla,dc=com"

Recherchez la dans l'annuaire.


5.4/ Modifier une entrée
Notre entrée est bien, mais je me suis trompé dans le nom. Son nom est Bender B. Rodriguez.
Modifions l'entrée:
# cat modify.ldif 
dn: uid=bender,ou=people,dc=bla,dc=com
changetype: modify
replace: cn
cn: Bender B. RODRIGUEZ
 
# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f modify.ldif -w bla
modifying entry "uid=bender,ou=people,dc=bla,dc=com"

NB: La commande ldapmodify permet de faire aussi des ajouts/suppressions.


5.5/ Supprimer une entrée
Supprimons l'entrée uid=bender,ou=people,dc=bla,dc=com (plusieurs manières de possible):
# ldapdelete -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -w bla uid=bender,ou=people,dc=bla,dc=com
ou
# cat delete.ldif
dn: uid=bender,ou=people,dc=bla,dc=com
changetype: delete

# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f delete.ldif -w bla
deleting entry "uid=bender,ou=people,dc=bla,dc=com"


5.6/ Ajouter une entrée avec ldapmodify
# cat bender-modify.ldif
dn: uid=bender,ou=people,dc=bla,dc=com
changetype: add
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
sn: RODRIGUEZ
givenName: Bender
cn: RODRIGUEZ Bender
userPassword: bender
homeDirectory: /home/bender
uidNumber: 1101
gidNumber: 1101
loginShell: /bin/bash
uid: bender


# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f bender-modify.ldif -w bla
adding new entry "uid=bender,ou=people,dc=bla,dc=com"


# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f rename.ldif -w bla
modifying entry "uid=bender,ou=people,dc=bla,dc=com"
ldap_modify: Naming violation (64)
        additional info: value of naming attribute 'uid' is not present in entry

Le principe était bien, mais ce n'est pas bon :)
Il faut utiliser la commande ldapmodrdn:
# ldapmodrdn -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com uid=bender,ou=people,dc=bla,dc=com uid=ivrogne -w bla

Verifiez votre entrée.

Remarque: Il y a deux fois le même attribut. supprimons en un :)


5.8/ Suppression d'attribut
# cat delete-attr.ldif 
dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modify
delete: uid
uid: bender

# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f delete-attr.ldif -w bla
modifying entry "uid=ivrogne,ou=people,dc=bla,dc=com"


5.9/ Ajouter un attribut
Ajoutons une description à notre entrée:
# cat add-attr.ldif 
dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modify
add: description
description: Kill All Humans  
# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f add-attr.ldif -w bla
modifying entry "uid=ivrogne,ou=people,dc=bla,dc=com"

 
5.10/ Opérations multiples
Supprimez l'entrée uid=ivrogne, et ajoutez à nouveau l'entrée uid=bender et renommer la en uid=ivrogne.
Voici deux ldifs différents mais qui font la même chose que ce qu'on vient de voir:
# cat operations-multiples1.ldif 
dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modify
delete: uid
uid: bender

dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modify
add: description
description: Kill All Humans

# cat operations-multiples2.ldif 
dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modify
delete: uid
uid: bender
-
add: description
description: Kill All Humans  

Modifiez votre entrée: 

# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f operations-multiples1.ldif -w bla
modifying entry "uid=ivrogne,ou=people,dc=bla,dc=com"

modifying entry "uid=ivrogne,ou=people,dc=bla,dc=com"

OU

# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f operations-multiples2.ldif -w bla
modifying entry "uid=ivrogne,ou=people,dc=bla,dc=com"


Maintenant que nous avons mis une entrée dans une "ou" people, nous allons créer un groupe associé.
Le but est de simuler un User et Group unix. 



5.12/ Ajouter d'un groupe et lié une entrée avec
# cat ~/group.ldif 
dn: cn=linux,ou=group,dc=bla,dc=com
cn: linux
gidNumber: 1200
memberUid: ivrogne
objectClass: top
objectClass: posixGroup


5.13/ Autre manière de renommer une entrée
# cat renamedn.ldif
dn: uid=ivrogne,ou=people,dc=bla,dc=com
changetype: modrdn
newrdn: uid=homer
deleteoldrdn: 1

# ldapmodify -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -f renamedn.ldif -w bla

Renommez la en "ivrogne" pour la suite du TP, de la manière de votre choix.

6/ Chiffrement LDAPS

Créez dans un premier temps la clé rsa:
# mkdir /etc/ldap/ssl/ && cd /etc/ldap/ssl/
# openssl genrsa -out ldap.key 4096
Generating RSA private key, 4096 bit long modulus
.....++
...................................................................++
e is 65537 (0x10001)

Créez le Certificate Signing Request, mettez les informations de votre choix:
# openssl req -new -key ldap.key -out ldap.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:

Signez votre certificat avec votre clé:
# openssl x509 -req -days 365 -in ldap.csr -signkey ldap.key -out ldap.crt
Signature ok
subject=/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd
Getting Private key

# ls
ldap.crt  ldap.csr  ldap.key


Changez les droits:
# chown openldap.openldap -R /etc/ldap/ssl/

Ajoutez les certificats dans la configuration de ldap
# cat ssl.ldif
dn: cn=config
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ldap/ssl/ldap.key
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ldap/ssl/ldap.crt

# ldapmodify -Y EXTERNAL -H ldapi:/// -f ssl.ldif 
SASL/EXTERNAL authentication started
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
SASL SSF: 0
modifying entry "cn=config"

Modifier le fichier /etc/default/slapd, juste cette ligne:
SLAPD_SERVICES="ldap:/// ldapi:///"
en
SLAPD_SERVICES="ldap:/// ldapi:/// ldaps:///"

Relancer slapd.
A ce stade le slapd écoute bien au port 389 (ldap) et 636 (ldaps)
mais il n'est pas encore possible d'interroger le serveur:

En Start_TLS:

# ldapsearch -x -h localhost -p 389 -D cn=admin,dc=bla,dc=com -b dc=bla,dc=com -w bla -ZZ

En SSL:

# ldapsearch -x -H ldaps:// -D cn=admin,dc=bla,dc=com -b dc=bla,dc=com -w bla

Vous avez des erreurs, il faut configuré le client ldap:

# echo "TLS_REQCERT allow" > /etc/ldap/ldap.conf

Relancer les recherches, ça doit être bon :)


7/ Pourquoi avoir fait ça ?
Connectons notre machine au LDAP
# apt-get install libnss-ldapd
Répondez aux questions :)

Une fois installé:
# getent passwd ivrogne
ivrogne:x:1101:1101:RODRIGUEZ Bender:/home/bender:/bin/bash

# su -l ivrogne
Pas de répertoire, connexion avec HOME=/
ivrogne@localhost:~$

# id ivrogne
uid=1101(ivrogne) gid=1101 groupes=1101,1200(linux)

Nous pouvons utiliser notre entrée comme un utilisateur.


8/ Sauvegarde de votre annuaire
Une sauvegarde de votre annuaire consiste à faire un ldapsearch depuis votre base, et à rediriger le
flux stdin dans un fichier.

# ldapsearch -xLLL -h localhost -p 389 -D cn=admin,dc=bla,dc=com -b dc=bla,dc=com -w bla > backup.ldif

Maintenant desinstaller slapd et supprimer votre conf:
# cp -rp /etc/ldap/ /etc/ldap.bak
# apt-get remove slapd
# dpkg --purge slapd
# rm -rf /etc/ldap/

Installez slapd, reconfigurez le, et importez votre backup. 
Il faut faire un ldapadd de votre backup.ldif


9/ Filtres de recherche
Ajoutez plusieurs entrées de votre choix en supprimant des attributs pour certains,
en ajoutant pour d'autres.
Voici une liste d'attribut que vous pouvez rajouter:
mail, telephoneNumber, postalCode, postalAddress, title...
Mais aussi des attributs de mon schéma:
pseudo, age, games, steamAccount.
(Ne pas oublié l'objectClass=gamer dans l'entrée LDAP)


Exemple d'entrée:
dn: uid=homer,ou=people,dc=bla,dc=com
objectClass: top
objectClass: person
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
objectClass: gamer
sn: SIMPSON
givenName: Homer
cn: SIMPSON Homer
userPassword: homer
homeDirectory: /home/homer
uidNumber: 1105
gidNumber: 1105
loginShell: /bin/bash
uid: homer
pseudo: Donut777
age: 43
games: Borderlands 2
games: Guild Wars
games: MineCraft
steamAccount: homer.simpson
mail: homer.simpson@fox.com


Pour plus d'informations sur les attributs, rendez dans vous /etc/ldap/schema/ 

Vous pouvez faire des recherches avec des filtres, voici la syntax:
(Boolean-operator(filter)(filter)(filter)...)

Les operators: 
AND &
OR |
NOT !

Filtres:
Equality    = 
Greater than or equal to    >= 
Less than or equal to   <= 
Presence    =* 
Approximate     ~= 


Par example:
uid=bender                                 -> affiche les entrées avec l'uid=bender
"(!(loginShell=/bin/bash))"                -> affiche toutes les entrées qui ne possèdent pas de loginShell=/bin/bash
"(&(objectClass=person)(uid=bender))"      -> affiche toutes les entrées avec l'uid=bender et qui a un objectClass=person.
"(|(objectClass=person)(objectClass=top))" -> affiche toutes les entrées avec objectClass=person ou objectClass=top
"mail=*"                                   -> affiche toutes les entrées qui possèdent un attribut mail 
etc ...


1 - Affichez les entrées de type person avec une adresse mail
2 - Affichez deux entrées de votre choix
3 - Affichez tous les groupes de uid=bender
4 - Affichez les personnes qui ont un compte Steam

Libérez votre imagination pour les filtres :)

